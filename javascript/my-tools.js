
function hello(name) {
  if(name.includes("zz")) {
    throw Error("😡 error name: " + name)
  } else {
    return "😀 hello " + name
  }
}

function hello2(name) {
  if(name.includes("Xw")) {
    throw Error("🥶 error name: " + name)
  } else {
    return "😀 hello " + name
  }
}

function hello3(name) {
  if(name.includes("fuck")) {
    throw Error("🤬 error name: " + name)
  } else {
    return "😀 hello " + name
  }
}

module.exports = {
  hello, hello2, hello3
};
